export type ServiceError = Partial<{
  code: string;
  status: number;
  statusCode: number;
  message: string;
}>;

export class HttpError extends Error {
  public error: ServiceError;
  constructor(message: string, error?: any) {
    super(message || 'Unexpected Error');
    this.error = error;
    Error.captureStackTrace(this, HttpError);
  }
}
