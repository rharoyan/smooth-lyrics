export class Urls {
  static readonly BASE_URL = 'https://api.happi.dev/v1';
  static readonly TRENDING_TRACKS = `music/artists/5787/smart-playlist/acrostic?message=importio`;

  static readonly LYRICS = (
    artistId: string,
    albumId: string,
    trackId: string
  ) => {
    return `/music/artists/${artistId}/albums/${albumId}/tracks/${trackId}/lyrics`;
  };

  static readonly HAPPI_KEY =
    '0ca9272QrphK7UnTtcUcLF7DCtHu2A96KSaiRBR147ze6umZ10epOH5I';
}
