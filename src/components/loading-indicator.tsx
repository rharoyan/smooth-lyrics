import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

type LoadingIndicatorProps = {
  isLoading: boolean;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    circularProgress: {
      color: '#000',
      marginLeft: '50%',
    },
  })
);

const LoadingIndicator = ({ isLoading }: LoadingIndicatorProps) => {
  const classes = useStyles();

  if (!isLoading) return <div></div>;
  return <CircularProgress className={classes.circularProgress} size={24} />;
};

export default LoadingIndicator;
