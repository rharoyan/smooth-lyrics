import React from 'react';

type TrackLyricsProps = {
  title: string;
  lyrics: string;
};

const TrackLyrics = ({ title, lyrics }: TrackLyricsProps) => {
  return (
    <React.Fragment>
      <h3>{title}</h3>
      <div
        style={{
          whiteSpace: 'pre-line',
        }}
      >
        {lyrics}
      </div>
    </React.Fragment>
  );
};

export default TrackLyrics;
