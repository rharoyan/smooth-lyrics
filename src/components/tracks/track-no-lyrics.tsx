import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const TrackNoLyrics = () => {
  return (
    <React.Fragment>
      <h3>No Lyrics found. Please search another song</h3>
      <Link to="/">
        <Button variant="outlined">Back to home</Button>
      </Link>
    </React.Fragment>
  );
};

export default TrackNoLyrics;
