import React from 'react';
import { Link } from 'react-router-dom';

import { Track } from '../../models/track';

import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

import './tracks.css';

type TracksListItemProps = {
  track: Track;
  onClick: Function;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    inline: {
      display: 'inline',
    },
  })
);

const TracksListItem = ({ track, onClick }: TracksListItemProps) => {
  const classes = useStyles();

  return (
    <Link
      to={`/artists/${track.id_artist}/albums/${track.id_album}/tracks/${track.id_track}`}
    >
      <ListItem>
        <ListItemAvatar>
          <Avatar alt={track.track} src={track.cover} />
        </ListItemAvatar>
        <ListItemText
          primary={track.track}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                {`${track.artist} — `}
              </Typography>
              {track.album}
            </React.Fragment>
          }
        />
        <ListItemSecondaryAction>
          <Button variant="outlined">See lyrics</Button>
        </ListItemSecondaryAction>
      </ListItem>
    </Link>
  );
};

export default TracksListItem;
