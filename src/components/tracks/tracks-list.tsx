import React from 'react';

import { Track } from '../../models/track';

import { Theme } from '@material-ui/core/styles/createMuiTheme';
import makeStyles from '@material-ui/core/styles/makeStyles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import createStyles from '@material-ui/core/styles/createStyles';

import TracksListItem from './track-list-item';

type TracksListProps = {
  tracks: Track[];
  onClick: Function;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
  })
);

const TracksList = ({ tracks, onClick }: TracksListProps) => {
  const classes = useStyles();

  const listItems: JSX.Element[] = tracks.map((track: Track) => {
    return (
      <React.Fragment key={track.id_track}>
        <TracksListItem track={track} onClick={() => {}}></TracksListItem>
        <Divider variant="inset" component="li" />
      </React.Fragment>
    );
  });

  return <List className={classes.root}>{listItems}</List>;
};

export default TracksList;
