import * as React from 'react';
import { TextField } from '@material-ui/core';

import RegularButton from '../buttons/regular-button';

import './search-form.css';

const SearchForm = () => {
  return (
    <form id="search-form">
      <TextField
        className="search-textfield"
        id="search-textfield"
        label="Song name..."
        variant="filled"
      />
      <RegularButton
        title="Search"
        onClick={() => {}}
        isLoading={false}
      ></RegularButton>
    </form>
  );
};

export default SearchForm;
