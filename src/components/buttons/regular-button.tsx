import React from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

type RegularButtonProps = {
  title: string;
  onClick: Function;
  color?: string;
  backgroundColor?: string;
  isLoading: boolean;
};

const RegularButton = ({
  title,
  onClick,
  color = '#fff',
  backgroundColor = '#E6636D',
  isLoading = false,
}: RegularButtonProps) => {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      circularProgress: {
        color: color,
      },
      button: {
        backgroundColor: backgroundColor,
        color: color,
        borderRadius: '4px',
        padding: '15px 50px',
        fontWeight: 800,
        letterSpacing: '0em',
        textTransform: 'none',
        '&:hover': {
          backgroundColor: backgroundColor,
          borderColor: backgroundColor,
          boxShadow: 'none',
        },
      },
    })
  );
  const classes = useStyles();
  return (
    <Button
      className={classes.button}
      onClick={() => {
        onClick();
      }}
    >
      {!isLoading ? (
        title
      ) : (
        <CircularProgress className={classes.circularProgress} size={24} />
      )}
    </Button>
  );
};

export default RegularButton;
