export interface Track {
  track: string;
  id_track: number;
  artist: string;
  id_artist: number;
  id_album: number;
  album: string;
  haslyrics: boolean;
  cover: string;
  api_artist: string;
  api_albums: string;
  api_album: string;
  api_tracks: string;
  api_track: string;
  api_lyrics: string;
  lyrics: string;
}
