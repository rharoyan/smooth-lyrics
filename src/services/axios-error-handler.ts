import { AxiosError, AxiosResponse } from 'axios';
import { HttpError } from '../utils/error/http-error';

export const handleNetworkError = (e: AxiosError) => {
  if (
    e.code === 'ECONNABORTED' ||
    e.message === 'Network Error' ||
    (e.response && e.response.status === 0)
  ) {
    return 0; // TODO: maybe we should catch more here
  }
  throw e;
};

const handleAxiosErrorResponse = (response: AxiosResponse): never => {
  const resData = response.data;
  if (resData && resData.message) {
    throw new HttpError(resData.message, {
      status: response.status,
      ...resData,
    });
  }
  throw new HttpError(response.statusText, {
    status: response.status,
    message: response.statusText,
    resData,
  });
};

export const handleError = (err: AxiosError): never => {
  if (err.isAxiosError && err.response) {
    handleAxiosErrorResponse(err.response);
  }
  throw err;
};
