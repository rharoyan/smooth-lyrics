import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { handleError } from './axios-error-handler';

export interface RestClient {
  get<T = unknown, R = T>(url: string): Promise<R>;

  delete<T = unknown, R = void>(url: string): Promise<R>;

  head<T = unknown>(url: string): Promise<T>;

  post<T = unknown, R = void>(url: string, data?: T): Promise<R>;

  put<T = unknown, R = void>(url: string, data?: T): Promise<R>;

  patch<T = unknown, R = void>(url: string, data?: T): Promise<R>;
}

export default class AxiosRestClient implements RestClient {
  instance: AxiosInstance;

  public constructor(config: AxiosRequestConfig) {
    this.instance = axios.create({
      ...config,
    });
  }

  public async get<T = unknown, R = T>(
    url: string,
    config?: AxiosRequestConfig
  ): Promise<R> {
    try {
      const response = await this.instance.get<T, AxiosResponse<R>>(
        url,
        config
      );
      return response.data;
    } catch (err) {
      return handleError(err);
    }
  }

  public async delete<T = unknown, R = void>(
    url: string,
    config?: AxiosRequestConfig
  ): Promise<R> {
    try {
      const response = await this.instance.delete<T, AxiosResponse<R>>(
        url,
        config
      );
      return response.data;
    } catch (err) {
      return handleError(err);
    }
  }

  public async head<T = unknown>(
    url: string,
    config?: AxiosRequestConfig
  ): Promise<T> {
    try {
      const response = await this.instance.head<T>(url, config);
      return response.data;
    } catch (err) {
      return handleError(err);
    }
  }

  public async post<T = unknown, R = void>(
    url: string,
    data?: T,
    config?: AxiosRequestConfig
  ): Promise<R> {
    try {
      const response = await this.instance.post<T, AxiosResponse<R>>(
        url,
        data,
        config
      );
      return response.data;
    } catch (err) {
      return handleError(err);
    }
  }

  public async put<T = unknown, R = void>(
    url: string,
    data?: T,
    config?: AxiosRequestConfig
  ): Promise<R> {
    try {
      const response = await this.instance.put<T, AxiosResponse<R>>(
        url,
        data,
        config
      );
      return response.data;
    } catch (err) {
      return handleError(err);
    }
  }

  public async patch<T = unknown, R = void>(
    url: string,
    data?: T,
    config?: AxiosRequestConfig
  ): Promise<R> {
    try {
      const response = await this.instance.patch<T, AxiosResponse<R>>(
        url,
        data,
        config
      );
      return response.data;
    } catch (err) {
      return handleError(err);
    }
  }
}
