import AxiosRestClient from '../axios-rest-client';
import { Track } from '../../models/track';
import { Urls } from '../../utils/urls';

export default class MusicService {
  client: AxiosRestClient;

  constructor(axiosClient: AxiosRestClient) {
    this.client = axiosClient;
  }

  async getLyrics(
    artistId: string,
    albumId: string,
    trackId: string
  ): Promise<Track> {
    const response = (await this.client.get(
      Urls.LYRICS(artistId, albumId, trackId)
    )) as any;
    const result = response.result as Track;
    return result;
  }

  async getTrendingTracks(): Promise<Track[]> {
    const response = (await this.client.get(Urls.TRENDING_TRACKS)) as any;
    const result = response.result as Array<Track>;
    return result;
  }
}
