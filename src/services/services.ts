import MusicService from './music/music-service';
import AxiosRestClient from './axios-rest-client';
import { Urls } from '../utils/urls';

const music = new MusicService(
  new AxiosRestClient({
    baseURL: Urls.BASE_URL,
    headers: {
      'Content-Type': 'application/json',
      'x-happi-key': Urls.HAPPI_KEY,
    },
  })
);

const services = {
  music,
};

export default services;
