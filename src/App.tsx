import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';

import HomePage from './pages/Home';
import TrackPage from './pages/Track';
import Page404 from './pages/Page404';

import './App.css';

function App() {
  return (
    <Router>
      <main>
        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route
            path="/artists/:artistId/albums/:albumId/tracks/:trackId"
            exact
          >
            <TrackPage />
          </Route>
          <Route path="/404" exact>
            <Page404 />
          </Route>
          <Redirect to="/404" />
        </Switch>
      </main>
    </Router>
  );
}

export default App;
