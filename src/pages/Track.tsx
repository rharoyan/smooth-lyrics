import * as React from 'react';
import { useState } from 'react';
import { useParams } from 'react-router-dom';

import Container from '@material-ui/core/Container';

import services from '../services/services';
import { Track } from '../models/track';

import Logo from '../components/logo';
import SearchForm from '../components/search-form/search-form';
import TrackLyrics from '../components/tracks/track-lyrics';
import TrackNoLyrics from '../components/tracks/track-no-lyrics';
import LoadingIndicator from '../components/loading-indicator';

interface TrackPageRouterProps {
  artistId: string;
  albumId: string;
  trackId: string;
}

const TrackPage = () => {
  const params = useParams<TrackPageRouterProps>();

  const [isLoading, setIsloading] = useState(false);
  const [track, setTrack] = useState({} as Track);
  const [hasNoLyrics, setHasNoLyrics] = useState(false);

  const _getLyrics = async () => {
    setIsloading(true);
    try {
      const result = await services.music.getLyrics(
        params.artistId,
        params.albumId,
        params.trackId
      );
      setTrack(result);
    } catch (error) {
      setHasNoLyrics(true);
    }
    setIsloading(false);
  };

  React.useState(() => {
    _getLyrics();
  });

  const _buildLyrics = () => {
    if (hasNoLyrics) {
      return <TrackNoLyrics />;
    }
    return (
      <TrackLyrics
        title={`${track.track ?? ''} — ${track.artist ?? ''}`}
        lyrics={track.lyrics}
      />
    );
  };

  return (
    <Container maxWidth="md">
      <Logo />
      <SearchForm />
      <LoadingIndicator isLoading={isLoading} />
      {_buildLyrics()}
    </Container>
  );
};

export default TrackPage;
