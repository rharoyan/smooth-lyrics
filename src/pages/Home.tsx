import * as React from 'react';
import { useState } from 'react';

import services from '../services/services';
import { Track } from '../models/track';

import Container from '@material-ui/core/Container';

import SearchForm from '../components/search-form/search-form';
import TracksList from '../components/tracks/tracks-list';
import Logo from '../components/logo';
import LoadingIndicator from '../components/loading-indicator';

const HomePage = () => {
  const [isLoading, setIsloading] = useState(false);
  const [trendingTracks, setTrendingTracks] = useState(new Array<Track>());

  React.useState(async () => {
    setIsloading(true);
    const result = await services.music.getTrendingTracks();
    setTrendingTracks(result);
    setIsloading(false);
  });

  return (
    <Container maxWidth="md">
      <Logo />
      <SearchForm />
      <LoadingIndicator isLoading={isLoading} />
      <TracksList tracks={trendingTracks} onClick={() => {}}></TracksList>
    </Container>
  );
};

export default HomePage;
