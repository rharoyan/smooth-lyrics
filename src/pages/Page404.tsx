import * as React from 'react';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';

const Page404 = () => {
  return (
    <React.Fragment>
      <h1>404 Page Not Found</h1>
      <div>
        <p>The page you were looking for does not exist.</p>

        <Link to="/">
          <Button variant="outlined">Back to home</Button>
        </Link>
      </div>
    </React.Fragment>
  );
};

export default Page404;
